<?php

namespace KDA\Laravel\Package;

class RawPath {

    public function __construct(public readonly string $path){}

    public static function from($path):static
    {
        return new static($path);
    }
}