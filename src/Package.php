<?php

namespace KDA\Laravel\Package;

use Illuminate\Support\Traits\Conditionable;
use Illuminate\Support\Traits\Tappable;
use KDA\Laravel\Package\Concerns\CanBoot;
use KDA\Laravel\Package\Concerns\CanHandlePath;
use KDA\Laravel\Package\Concerns\CanLoadRoutes;
use KDA\Laravel\Package\Concerns\CanLoadTrait;
use KDA\Laravel\Package\Concerns\CanPublishFiles;
use KDA\Laravel\Package\Concerns\CanRegister;
use KDA\Laravel\Package\Concerns\CanRegistersFacade;
use KDA\Laravel\Package\Concerns\HasMigrations;
use KDA\Laravel\Package\Concerns\HasName;
use KDA\Laravel\Package\Concerns\Laravel;
use KDA\Laravel\Package\Concerns\RegistersViews;

abstract class Package{
  
    use HasName;
    use CanHandlePath;
    use RegistersViews;
    use CanRegister;
    use CanRegistersFacade;
    use Laravel;
    use CanLoadTrait;
    use CanBoot;
    use CanPublishFiles;
    use HasMigrations;
    use Tappable;
    use CanLoadRoutes;
    use Conditionable;
    public function __construct(protected $provider,public string $name)
    {
        $this->loadLoadTraits();
        $this->setUp();
    }

    protected function setUp():void
    {

    }

    public static function make($provider,?string $name =null): static
    {
        $name ??= static::$package_name;
        app()->singleton($name, function () use ($provider,$name) {
            return new static($provider,$name);
        });
        return app($name);
    }

    public static function get(?string $name =null): static
    {
        $name ??= static::$package_name;
        return app($name);
    }

}