<?php

namespace KDA\Laravel\Package\Concerns;

use KDA\Laravel\Package\RawPath;

trait CanHandlePath
{
    
    public function getPackageBasePath()
    {
        $reflector = new \ReflectionClass(get_class($this));

        return dirname($reflector->getFileName(), 2);
    }

    public function getPath(RawPath | string $path)
    {
        if($path instanceof RawPath){
            return $path;
        }
        return app()->joinPaths($this->getPackageBasePath(), $path);
    }

    public function getRecursiveFilesInPath($path):array
    {
        $path = $this->getPath($path);
     
        $result = [];

        if (file_exists($path)) {
            $it = new \RecursiveDirectoryIterator($path);
            foreach (new \RecursiveIteratorIterator($it) as $file) {
                if ($file->isFile()) {
                    $result[] = $file->getPathname();
                }
            }
        }

        return $result;
    }
}
