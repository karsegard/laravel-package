<?php

namespace KDA\Laravel\Package\Concerns;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;

trait HasMigrations
{

    protected bool $should_publish_migrations = true;
    protected string $migration_publish_key = 'migrations';
    protected string $migration_path = 'database/migrations';
    protected $migration_timestamp_regex = '/([\d]{4}_[\d]{2}_[\d]{2}_[\d]{6}_)/m';

    /**
     * package setUp step or package register step
     */
    public function migrationPublishableKey(string $key):static
    {
        $this->migration_publish_key = $key;
        return $this;
    }
     /**
     * package setUp step or package register step
     */
    public function publishMigrations(bool $bool): static
    {
        $this->should_publish_migrations = $bool;
        return $this;
    }

    public function shouldPublishMigrations(): bool
    {
        return $this->should_publish_migrations;
    }

    public function bootHasMigrations(): void
    {
        if ($this->inConsole() && $this->shouldPublishMigrations()) {
            $this->registerPublishableMigrations();
        }
    }

    protected function registerPublishableMigrations(): void
    {
        $migrations = $this->getRecursiveFilesInPath($this->migration_path);
        foreach ($migrations as $migration) {
            $this->publish(
                key: $this->migration_publish_key,
                src: $migration,
                dest: $this->getMigrationFileName(basename($migration))
            );
        }
    }

    protected function getTimelessMigrationFileName($migrationFileName): string
    {
        return preg_replace($this->migration_timestamp_regex, '', $migrationFileName);
    }

    protected function migrationFileHasTimestamp($migrationFileName): bool
    {
        return preg_match($this->migration_timestamp_regex, $migrationFileName) !== false;
    }
    /*
       Get Publishable migration file name.
       update or add timestamp from package file name
    */
    protected function getMigrationFileName($migrationFileName): string
    {
        $timestamp = date('Y_m_d_His');

        $filesystem = $this->app()->make(Filesystem::class);
        $newFileName = $this->app()->databasePath() . "/migrations/{$timestamp}_{$migrationFileName}";
        if ($this->migrationFileHasTimestamp($migrationFileName)) {
            // filename already have a timestamp
            $newFileName =  $this->app()->databasePath() . "/migrations/" . preg_replace($this->migration_timestamp_regex, $timestamp . "_", $migrationFileName);
        }

        $timelessMigrationFileName = $this->getTimelessMigrationFileName($migrationFileName);
        return Collection::make($this->app()->databasePath() . DIRECTORY_SEPARATOR . 'migrations' . DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem, $timelessMigrationFileName) {
                return $filesystem->glob($path . '*_' . $timelessMigrationFileName);
            })
            ->push($newFileName)
            ->first();
    }
}
