<?php

namespace KDA\Laravel\Package\Concerns;

use Closure;

trait CanPublishFiles {

    protected array $publishables = [];
    

    public function publish($key,$src,$dest):static
    {
        $this->publishables[$key][$src] =$dest;
        return $this;
    }

    public function afterBootCanPublishFiles(){
        if ($this->inConsole()) {
            foreach ($this->publishables as $group => $paths) {
                
                $this->publishes($paths, $group);
            }
        }
    }

    protected function publishes(array $paths, $groups = null)
    {
        $provider = $this->provider::class;
                
        $this->ensurePublishArrayInitialized($class = $provider);

        $provider::$publishes[$class] = array_merge($provider::$publishes[$class], $paths);

        foreach ((array) $groups as $group) {
            $this->addPublishGroup($group, $paths);
        }
    }

     /**
     * Ensure the publish array for the service provider is initialized.
     *
     * @param  string  $class
     * @return void
     */
    protected function ensurePublishArrayInitialized($class)
    {
        $provider = $this->provider::class;
        if (! array_key_exists($class, $provider::$publishes)) {
            $provider::$publishes[$class] = [];
        }
    }

    /**
     * Add a publish group / tag to the service provider.
     *
     * @param  string  $group
     * @param  array  $paths
     * @return void
     */
    protected function addPublishGroup($group, $paths)
    {
        $provider = $this->provider::class;

        if (! array_key_exists($group, $provider::$publishGroups)) {
            $provider::$publishGroups[$group] = [];
        }

        $provider::$publishGroups[$group] = array_merge(
            $provider::$publishGroups[$group], $paths
        );
    }

    public function publishPath($key,$path,string | Closure $destination):static
    {
        $this->bootClosure(function() use ($key,$path,$destination){
            $files = $this->getRecursiveFilesInPath($path);
        
            foreach ($files as $file) {
           
                if(is_callable($destination)){
                    $dest = $this->app()->call($destination,['package'=>$this,'file'=>$file]);
                }else{
                    //$dest = $this->app()->joinPaths($destination,$file);
                    $relativeSourcePath = str_replace($this->getPath($path),'',$file);
                    $dest = base_path($this->app()->joinPaths($destination,$relativeSourcePath));
                }
                $this->publish($key,$file,$dest);
            }
        });
       
        return $this;
    }
}
