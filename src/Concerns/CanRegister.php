<?php

namespace KDA\Laravel\Package\Concerns;

use Closure;

trait CanRegister {
    protected array $register_closures = [];

    public function registerClosure(Closure $callback): void
    {
        $this->register_closures[] = $callback;
    }

    public function register(): void
    {
        $this->runLoadableHooks('register');

        array_map(
            fn (callable $hook): string => (string) app()->call($hook),
            $this->register_closures ?? [],
        );
        
    }
}
