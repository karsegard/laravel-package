<?php

namespace KDA\Laravel\Package\Concerns;

use Closure;

trait CanBoot {

    protected array $boot_closures = [];

    public function bootClosure(Closure $callback): void
    {
        $this->boot_closures[] = $callback;
    }
    
    public function boot()
    {
        $this->runLoadableHooks('beforeBoot');

        $this->runLoadableHooks('boot');
        array_map(
            fn (callable $hook): string => (string) app()->call($hook),
            $this->boot_closures ?? [],
        );
        $this->runLoadableHooks('afterBoot');

    }
}
