<?php

namespace KDA\Laravel\Package\Concerns;

trait CanLoadTrait {
    protected $traits_loadable_methods = [];

    
    protected function loadLoadTraits(){
       
        $traits = class_uses_recursive(static::class);
        foreach ($traits as $trait) {
            $name = (string)str($trait)->afterLast('\\');
          //  $this->traits_boot[] = "boot" . $name;
            //$this->traits_register[] = "register" . $name;
            $this->registerLoadableMethod('boot',$name);
            $this->registerLoadableMethod('afterBoot',$name);
            $this->registerLoadableMethod('beforeBoot',$name);
            $this->registerLoadableMethod('register',$name);
            $init = "load" . $name;
            if (method_exists($this, $init)) {
                $this->$init();
            }
        }
    }

    protected function registerLoadableMethod($hook,$name)
    {
        $name = $hook.$name;
        if (method_exists($this, $name)) {
            $this->traits_loadable_methods[$hook][]=$name;
        }
    }

    protected function runLoadableHooks($hook){
        array_map(
            fn($hook)=> $this->$hook(),
            $this->traits_loadable_methods[$hook] ?? []
        );
    }

   /* protected function runTraitsRegister(){
        foreach ($this->traits_register as $register) {
                $this->$register();
        }
    }
    protected function runTraitsBoot(){
        foreach ($this->traits_boot as $boot) {
                $this->$boot();
        }
    }*/
}
