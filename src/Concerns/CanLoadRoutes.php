<?php

namespace KDA\Laravel\Package\Concerns;

use Closure;

trait CanLoadRoutes
{

    protected string $route_path = 'routes';

    public function routes(bool $publishable = false, string $publish_key = 'routes'):static
    {
       
        $this->bootClosure(function () use ($publishable, $publish_key) {
            $this->loadRoutes($publishable,  $publish_key);
        });
        return $this;
    }

    public function loadRoutes(bool $publishable = false,  string $publish_key = 'routes')
    {

        $files = $this->getRecursiveFilesInPath($this->route_path);

        $this->when($publishable,function()  use ($files, $publish_key){
            foreach($files as $f){
                $this->publish($publish_key,$f,$this->getPublishedRouteFilePath($f));
            }
        });

        foreach ($files as $route) {
            invade($this->provider)->loadRoutesFrom($this->getRealRouteFilePath($route));
        }
    }

    public function getPublishedRouteFilePath($file):string 
    {
        return base_path('routes/vendor/'.$this->getPackageName().'/' . basename($file));
    }
    /**
     * return the published path if published
     */
    public function getRealRouteFilePath($file):string
    {
        $published_path = $this->getPublishedRouteFilePath($file);
        if (file_exists($published_path)) {
            return $published_path;
        }
        return $file;
    }
}
