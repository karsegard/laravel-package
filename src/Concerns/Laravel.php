<?php

namespace KDA\Laravel\Package\Concerns;

trait Laravel {
    

    protected function app(){
        return app();
    }

    protected function inConsole():bool
    {
        return $this->app()->runningInConsole();
    }

}
