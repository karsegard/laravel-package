<?php

namespace KDA\Laravel\Package\Concerns;
use Illuminate\Support\Facades\Gate;

trait HasPolicies {
    
    public function policy($model,$policy):static
    {
        $this->bootClosure(function() use ($model,$policy){
            Gate::policy($model, $policy);
        });
        return $this;
    }
}
