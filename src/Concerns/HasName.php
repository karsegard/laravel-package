<?php

namespace KDA\Laravel\Package\Concerns;

trait HasName {
    protected static string $package_name;
    protected function getPackageName():string
    {
        return $this->name;
    }

}
