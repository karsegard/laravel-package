<?php

namespace KDA\Laravel\Package\Concerns;

use Closure;
use Illuminate\Support\Facades\Facade;

trait CanRegistersFacade {
    
    /**
     * Must be called in the register step of your package
     */
    public function facade(string $facade,string $lib):static
    {
        $this->registerClosure(function() use ($facade,$lib){
            app()->singleton($facade, function ()  use ($facade,$lib) {
                return new $lib();
            });
        });
        return $this;
    }
}
