<?php

namespace KDA\Laravel\Package\Concerns;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use KDA\Laravel\Package\RawPath;

trait RegistersViews
{
    protected string $view_publish_key = 'views';

    public function viewPublishableKey(string $key):static
    {
        $this->view_publish_key = $key;
        return $this;
    }

    public function views($publish = true): static
    {
        return $this->loadViewsFrom('resources/views')->when($publish, fn ($e) => $e->publishViews());
    }

    public function publishViews(?string $namespace = null, ?string $key = null): static
    {
        return $this->publishViewsFrom('resources/views', namespace: $namespace, key:$key);
    }

  /*  public function components(): static
    {
        return $this->loadComponentsFrom('resources/views/components');
    }
*/

    public function loadViewsFrom(RawPath | string $path, ?string $namespace = null): static
    {
        $namespace ??= $this->getPackageName();
        invade($this->provider)->loadViewsFrom($this->getPath($path),$namespace);
        //View::addNamespace($namespace, $this->getPath($path));
        return $this;
    }

    /*public function loadComponentsFrom(RawPath | string $path, ?string $namespace = null): static
    {
        $namespace ??= $this->getPackageName();
        Blade::anonymousComponentPath($this->getPath($path), $namespace);
        Blade::anonymousComponentNamespace($this->getPath($path), $namespace);
       // dump(invade(app('blade.compiler'))->anonymousComponentPaths);
        return $this;
    }
*/
    public function publishViewsFrom($path, ?string $namespace = null, ?string $key = null): static
    {
        $key ??= $this->view_publish_key;
        if ($this->inConsole()) {
            $namespace ??= $this->getPackageName();
            $this->publishPath($key, $path, function ($file) use ($path,$namespace) {
                return str_replace($this->getPath($path), resource_path('/views/vendor/' . $namespace), $file);
            });
        }
        return $this;
    }

    
}
